#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <time.h>
#include <pthread.h>
#include "ini.h"
#include "worker.h"
#include "server.h"

config g_config_server;
int g_epoll_list_fd;

struct epoll_event g_event[SERVER_MAX_CONNECTION] = {0};
connection g_connections[SERVER_MAX_CONNECTION] = {0};

static void server_initialize_connection(int socket_r, int socket_w);
static int server_add_socket_to_epoll(int r_socket, int w_socket);
static void server_init_sockaddr (struct sockaddr_in *name, const char *hostname, uint16_t port);
static int server_socket_set_non_blocking(int socket);
static int server_socket_prepare(int socket, struct sockaddr_in *address, char *ip, int port);
static int server_socket_connect(int socket, struct sockaddr_in *address);
static int server_socket_bind(int socket, struct sockaddr_in *address);
static int server_modify_epoll_flag(int socket, int flag);
static void server_check_for_timeout(void);

int server_start(void)
{
    int server_fd, new_socket;
    struct sockaddr_in address;
    struct sockaddr_in upstream;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
       perror("socket failed");
       exit(EXIT_FAILURE);
    }

    if (server_socket_prepare(server_fd, &address, g_config_server.server.ip, g_config_server.server.port) != OK)
        exit(EXIT_FAILURE);

    if (server_socket_bind(server_fd, &address) != OK)
        exit(EXIT_FAILURE);

    listen(server_fd, 1000);
    while(TRUE)
    {
        new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
        if (new_socket >= 0)
        {
            int upstream_socket;
            int addrlen = sizeof(upstream);

            if ((upstream_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
            {
                perror("socket failed");
            }
            server_socket_prepare(upstream_socket, &upstream, g_config_server.upstream.ip, g_config_server.upstream.port);
            server_socket_connect(upstream_socket, &upstream);
            server_initialize_connection(new_socket, upstream_socket);
            server_initialize_connection(upstream_socket, new_socket);
            g_connections[upstream_socket].upstream = 1;
            g_connections[new_socket].upstream = 0;
            server_add_socket_to_epoll(new_socket, upstream_socket);
        }
        else
        {
            server_check_for_timeout();
        }
    }
    return OK;
}

int server_load_config(char *config_path)
{
	const char *tmp = "";
	ini_t *ini = ini_load(config_path);

	strncpy(g_config_server.server.protocol, ini_get(ini, "server", "protocol"), sizeof(g_config_server.server.protocol));

	tmp = ini_get(ini, "server", "ip");
	if (tmp == NULL)
	 return NO;
	strncpy(g_config_server.server.ip, tmp, sizeof(g_config_server.server.ip));

	tmp = ini_get(ini, "server", "port");
	if (tmp == NULL)
	 return NO;
	g_config_server.server.port = atoi(tmp);

	tmp = ini_get(ini, "upstream", "ip");
	if (tmp == NULL)
	 return NO;
	strncpy(g_config_server.upstream.ip, tmp, sizeof(g_config_server.upstream.ip));

	tmp = ini_get(ini, "upstream", "port");
	if (tmp == NULL)
	 return NO;
	g_config_server.upstream.port = atoi(tmp);

	tmp = ini_get(ini, "upstream", "timeout");
	if (tmp == NULL)
	 return NO;
	g_config_server.upstream.timeout = atoi(tmp);

	printf("[Server]\n");
	printf("Protocol = %s\n", g_config_server.server.protocol);
	printf("Ip: %s Port: %d\n", g_config_server.server.ip, g_config_server.server.port);
	printf("[Upstream]\n");
	printf("Ip: %s Port: %d\n", g_config_server.upstream.ip, g_config_server.upstream.port);

	ini_free(ini);
	return OK;
}

static void server_initialize_connection(int socket_r, int socket_w)
{
    printf("SOCKET r = %d && SOCKET_out = %d\n", socket_r, socket_w);
    g_connections[socket_r].socket_in = socket_r;
    g_connections[socket_r].size = 0;
    g_connections[socket_r].time = time(NULL);
    g_connections[socket_r].buffer = NULL;
    g_connections[socket_r].socket_out = socket_w;
}

static int server_add_socket_to_epoll(int r_socket, int w_socket)
{
    printf("r_socket = %d\n", r_socket);
    printf("w_socket = %d\n", w_socket);
    g_event[r_socket].data.fd = r_socket;
    g_event[r_socket].events = EPOLLIN;
    if (epoll_ctl(g_epoll_list_fd, EPOLL_CTL_ADD, r_socket, g_event + r_socket) == -1)
    {
        perror ("add socket_in epoll_ctl\n");
        return NO;
    }

    g_event[w_socket].data.fd = w_socket;
    g_event[w_socket].events = EPOLLIN;
    if (epoll_ctl(g_epoll_list_fd, EPOLL_CTL_ADD, w_socket, g_event + w_socket) == -1)
    {
        perror ("add socket_out epoll_ctl\n");
        return NO;
    }

    return OK;
}

static void server_init_sockaddr(struct sockaddr_in *name, const char *hostname, uint16_t port)
{
   name->sin_family = AF_INET;
   name->sin_port = htons(port);
   name->sin_addr.s_addr = inet_addr(hostname);
}

static int server_socket_set_non_blocking(int socket)
{
    int flags, s;

    flags = fcntl(socket, F_GETFL, 0);
    if (flags == -1)
    {
        perror ("fcntl");
        return NO;
    }

    flags |= O_NONBLOCK;
    s = fcntl (socket, F_SETFL, flags);
    if (s == -1)
    {
       perror ("fcntl");
       return NO;
    }
    return OK;
}

static int server_socket_prepare(int socket, struct sockaddr_in *address, char *ip, int port)
{
    int opt = 1;
    server_socket_set_non_blocking(socket);

    if (setsockopt(socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
       perror("setsockopt");
       return NO;
    }
    printf("ip = %s\n", ip);
    printf("port = %d\n", port);
    server_init_sockaddr(address, ip, port);

    return OK;
}

static int server_socket_connect(int socket, struct sockaddr_in *address)
{
    if (connect(socket, (struct sockaddr *)address, sizeof(*address)) == 0)
    {
        perror("error on connect");
        return NO;
    }
    return OK;
}

static int server_socket_bind(int socket, struct sockaddr_in *address)
{
    if (bind(socket, (struct sockaddr *)address, sizeof(*address)) < 0)
    {
        perror("bind failed");
        return NO;
    }
    return OK;
}

static int server_modify_epoll_flag(int socket, int flag)
{
    g_event[socket].data.fd = socket;
    g_event[socket].events = flag;
    if (epoll_ctl(g_epoll_list_fd, EPOLL_CTL_MOD, socket, g_event + socket) == -1)
    {
        perror ("add socket_in epoll_ctl\n");
        return NO;
    }
    return OK;
}

static void server_check_for_timeout(void)
{
    int i;
    for (i = 0; i < SERVER_MAX_CONNECTION; i++)
    {
        time_t t = time(NULL);
        if(g_connections[i].upstream == 1 && (t - g_connections[i].time > g_config_server.upstream.timeout))
        {
            printf("CLOSE socket : %d\n", g_connections[i].socket_in);
            server_free_buffer_connection(i);
            close(g_connections[i].socket_in);
        }
    }
}

void server_epoll_init(void)
{
    g_epoll_list_fd = epoll_create1(0);
}

void server_init_worker(void)
{
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, worker_start, NULL);
}

int server_check_for_write(int socket)
{
	int rsocket = g_connections[socket].socket_out;
    if (g_connections[rsocket].size > 0)
        return OK;

    return NO;
}

int server_write_to_socket(int socket)
{
	int rsocket = g_connections[socket].socket_out;
	int result = send(socket, g_connections[rsocket].buffer, g_connections[rsocket].size, MSG_DONTWAIT);
    printf("buffer = %s send on %d socket..\n", (char*)(g_connections[rsocket].buffer), socket);
	if (result < 0)
	{
		perror("send err\n");
	}
	else if (result > 0)
	{
		g_connections[socket].size = 0;
		server_modify_epoll_flag(socket, EPOLLIN);
	}

	return result;
}

int server_read_from_socket(int socket)
{
    int recieve_len = 0;
    while (1)
    {
        int result = 0;
        char buff[1024] = {0};
        result = recv(socket, buff, sizeof(buff), MSG_DONTWAIT);
        if (result == -1 || result == 0)
            break;

        if (result > 0)
        {
            {
                g_connections[socket].buffer = (void *)realloc(g_connections[socket].buffer, result + g_connections[socket].size);
                memcpy(g_connections[socket].buffer + g_connections[socket].size, buff, result);
                g_connections[socket].size += result;
            }
        }
        recieve_len += result;
    }

    if (recieve_len > 0)
	{
	    g_connections[socket].time = time(NULL);
	    server_modify_epoll_flag(g_connections[socket].socket_out, EPOLLIN | EPOLLOUT);
    }
    return recieve_len;
}

void server_free_buffer_connection(int socket)
{
    free(g_connections[socket].buffer);
    memset(&g_connections[socket], 0, sizeof(connection));
}
