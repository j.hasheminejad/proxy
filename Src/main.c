/*
 * main.c
 *
 *  Created on: Sep 17, 2020
 *      Author: javad
 */
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "server.h"


int main(int argc, char **argv)
{
    char config_path[256] = {0};

    if (argc > 1)
    {
    	strcpy(config_path, argv[1]);
    }
    else
    {
        printf("please enter ini config file and try again\n");
        exit(0);
    }
    if (server_load_config(config_path) == 0)
    {
        server_epoll_init();
        server_init_worker();
        server_start();
    }

    exit(0);
}
