/*
 * worker.c
 *
 *  Created on: Sep 17, 2020
 *      Author: javad
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include "server.h"

extern int g_epoll_list_fd;

void *worker_start(void *param)
{
    struct epoll_event result_event[SERVER_MAX_CONNECTION] = {0};
    while(TRUE)
    {
        int i;
        int j;
        memset(result_event, 0, sizeof(result_event));
        int event_count = epoll_wait(g_epoll_list_fd, result_event, SERVER_MAX_CONNECTION, 10);
        if (event_count == -1)
        {
            perror("epoll_wait");
            continue;
        }
        for (i = 0; i < event_count; ++i)
        {
            int socket = result_event[i].data.fd;
            if (result_event[i].events & (EPOLLHUP | EPOLLERR))
            {
                server_free_buffer_connection(result_event[i].data.fd);
                continue;
            }

            if (result_event[i].events & EPOLLIN)
                server_read_from_socket(socket);

            if (result_event[i].events & EPOLLOUT)
            {
                if (server_check_for_write(socket) == 0)
                    server_write_to_socket(socket);
            }
        }
    }
    return OK;
}
