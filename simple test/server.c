#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 

#define PORT 9000
#define SA struct sockaddr 

int main() 
{ 
	int sockfd, connfd, len; 
	struct sockaddr_in servaddr, cli; 

	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1)
	{
		printf("socket creation failed...\n"); 
		exit(-1);
	} 
	memset(&servaddr, 0, sizeof(servaddr));

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORT); 

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0)
	{
		printf("socket bind failed...\n"); 
		exit(-1);
	} 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 1000)) != 0)
	{
		printf("Listen failed...\n"); 
		exit(-1);
	} 
	len = sizeof(cli); 

	while(1)
	{
		connfd = accept(sockfd, (SA*)&cli, &len);
		if (connfd < 0)
		{
			printf("server acccept failed...\n");
			exit(-1);
		}

		receive_send(connfd);
	}
	close(sockfd); 
} 

void receive_send(int sockfd)
{
    int i;
    for (i = 0; i < 10; i++)
    {
        char buff[1024] = {0};
        read(sockfd, buff, sizeof(buff));
        //printf("From client: %s \n To client %s: \n", buff, buff);
        write(sockfd, buff, sizeof(buff));
    }
}
