#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 

#define SA struct sockaddr
char input_buffer[1024] = {0};

int main(int argc, char **argv)
{ 
    if (argc > 1)
    {
    	strcpy(input_buffer, argv[1]);
    }
    else
    {
        printf("please enter ini config file and try again\n");
        exit(-2);
    }
	int sockfd, sockfd_2;
	struct sockaddr_in servaddr, cli; 

	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1)
	{
		printf("socket creation failed...\n"); 
		return(-2);
	} 
	else
	{
		printf("Socket successfully created..\n");
	}

	memset(&servaddr, 0, sizeof(servaddr));
	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	servaddr.sin_port = htons(8080);

	// connect the client socket to server socket 
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0)
	{
		printf("connection with the server failed...\n"); 
		return(-2);
	} 
	else
	{
		printf("connected to the server..\n");
	}
	if (send_receive(sockfd) != 0)
	{
		close(sockfd);
		return(-2);
	}
	close(sockfd); 
	return 0;
} 

int send_receive(int sockfd)
{
    int i;
	for (i = 0; i < 10; i++)
	{
		char recv_buff[1024] = {0};
		char send_buff[1024] = {0};
	    sprintf(send_buff, "%s-%d", input_buffer, i);
		write(sockfd, send_buff, sizeof(send_buff));
		if (read(sockfd, recv_buff, sizeof(recv_buff)) > 0);
		{
		    printf("From Server : %s \n", recv_buff);
		    if (strcmp(send_buff, recv_buff) != 0)
		    {
		    	return -1;;
		    }
	    }
	}
	return 0;
}

