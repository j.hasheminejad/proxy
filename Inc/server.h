/*
 * server.h
 *
 *  Created on: Sep 17, 2020
 *      Author: javad
 */

#ifndef INCLUDE_SERVER_H_
#define INCLUDE_SERVER_H_

typedef struct
{
    char         protocol[10];
    char	     ip[25];
    int          port;
} server;

typedef struct
{
    int          timeout;
    char	     ip[25];
    int          port;
} upstream;

typedef struct
{
    server server;
    upstream upstream;
} config;

typedef struct
{
    int socket_in;
    int socket_out;
    int upstream;
    void *buffer;
    int size;
    time_t time;
} connection;

#define TRUE            1
#define FALSE           0
#define OK              0
#define NO              1

#define SERVER_MAX_CONNECTION 1024

int server_start(void);
int server_load_config(char *config_path);
void server_epoll_init(void);
void server_init_worker(void);
int server_check_for_write(int socket);
int server_write_to_socket(int socket);
int server_read_from_socket(int socket);
void server_free_buffer_connection(int socket);

#endif /* INCLUDE_SERVER_H_ */
