/*
 * worker.h
 *
 *  Created on: Sep 17, 2020
 *      Author: javad
 */

#ifndef INCLUDE_WORKER_H_
#define INCLUDE_WORKER_H_

void *worker_start(void *param);


#endif /* INCLUDE_WORKER_H_ */
